" Vim color file
" Maintainer: Brian Gomes Bascoy
" Last Change: 2019-02-26T14:01:09-0800

highlight clear

if exists("syntax_on")
  syntax reset
endif

let colors_name = "dilemma"

highlight Comment cterm=NONE ctermfg=DarkGrey ctermbg=NONE
highlight Constant cterm=NONE ctermfg=Magenta ctermbg=NONE
highlight Identifier cterm=bold ctermfg=Green ctermbg=NONE
highlight Statement cterm=NONE ctermfg=NONE ctermbg=NONE
highlight PreProc cterm=NONE ctermfg=NONE ctermbg=NONE
highlight Type cterm=bold ctermfg=Blue ctermbg=NONE
highlight Special cterm=NONE ctermfg=Red ctermbg=NONE
highlight Underlined cterm=underline ctermfg=Blue ctermbg=NONE
highlight Ignore cterm=NONE ctermfg=NONE ctermbg=NONE
highlight Error cterm=NONE ctermfg=White ctermbg=Red
highlight Todo cterm=NONE ctermfg=Black ctermbg=Yellow

highlight Visual cterm=reverse ctermfg=NONE ctermbg=NONE
highlight Search cterm=nocombine ctermfg=Black ctermbg=Yellow

highlight Directory cterm=NONE ctermfg=Cyan ctermbg=NONE
highlight MoreMsg cterm=NONE ctermfg=Green ctermbg=NONE
highlight Question cterm=NONE ctermfg=Green ctermbg=NONE
highlight SpecialKey cterm=NONE ctermfg=Blue ctermbg=NONE
highlight Title  cterm=NONE ctermfg=Magenta ctermbg=NONE
highlight WarningMsg cterm=NONE ctermfg=Red ctermbg=NONE
highlight Conceal cterm=NONE ctermfg=Grey ctermbg=NONE
